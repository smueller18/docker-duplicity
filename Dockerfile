FROM python:3.7-alpine

ARG SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v0.1.9/supercronic-linux-amd64
ARG SUPERCRONIC=supercronic-linux-amd64
ARG SUPERCRONIC_SHA1SUM=5ddf8ea26b56d4a7ff6faecdd8966610d5cb9d85

RUN apk add --no-cache --virtual .build-deps curl \
    && \
    curl -fsSLO "$SUPERCRONIC_URL" \
    && \
    echo "${SUPERCRONIC_SHA1SUM}  ${SUPERCRONIC}" | sha1sum -c - \
    && \
    chmod +x "$SUPERCRONIC"  && mv "$SUPERCRONIC" "/usr/local/bin/${SUPERCRONIC}" \
    && \
    ln -s "/usr/local/bin/${SUPERCRONIC}" /usr/local/bin/supercronic \
    && \
    apk del .build-deps

RUN apk add --no-cache gnupg librsync musl && \
    apk add --no-cache --virtual .build-deps \
       g++ \
       librsync-dev \
    && \
    pip install --no-cache-dir \
      requests \
      requests-oauthlib \
      https://code.launchpad.net/duplicity/0.8-series/0.8.02/+download/duplicity-0.8.02.tar.gz \
    && \
    apk del .build-deps \
    && \
    rm -f /var/spool/cron/crontabs/root

RUN sed -i 's/unicode(/str(/g' /usr/local/lib/python3.7/site-packages/duplicity/backends/onedrivebackend.py \
    && \
    sed -i "s/ti.gname = b''/ti.gname = u''/" /usr/local/lib/python3.7/site-packages/duplicity/path.py

RUN addgroup -g 101 -S duplicity && \
    adduser -u 101 -D -S -G duplicity duplicity

USER duplicity:duplicity

ENV HOME=/var/tmp
WORKDIR /var/tmp

VOLUME /var/tmp/.cache

CMD ["supercronic", "/var/spool/cron/crontabs/duplicity"]
